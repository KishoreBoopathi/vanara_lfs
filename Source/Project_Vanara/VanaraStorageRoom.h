// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "VanaraStorageRoom.generated.h"

USTRUCT(BlueprintType)
struct FStoryQuestDetails
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "QuestList")
	int32 StoryQuestNumber;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "QuestList")
	TSubclassOf<class AQuest> Quest;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "QuestList")
	TSubclassOf<AActor> QuestSpawnLocation;
};

UCLASS()
class PROJECT_VANARA_API AVanaraStorageRoom : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AVanaraStorageRoom();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "SaveData")
	TMap<FString, TSubclassOf<class AWeapon_Base>> WeaponMap;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "QuestList")
	TArray<FStoryQuestDetails> StoryQuestList;

};
